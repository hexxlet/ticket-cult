var myMap;

function initMap() {
    var element = document.getElementById('map');
    var options = {
        zoom : 12,
        center : {lat : 59.93863, lng : 30.31413},
        disableDefaultUI: true,
        fullscreenControl: true
    };

    myMap = new google.maps.Map(element, options);

    var text = loadJSON('place/get/all');
    var places = JSON.parse(text);
    places.forEach(function (place) {
        if (!lang) {
            addMarker(place.latitude, place.longitude, place.nameENG, place.id);
        }
        else {
            switch (lang){
                case 'en' :
                    addMarker(place.latitude, place.longitude, place.nameENG, place.id);
                    break;
                case 'ru' :
                    addMarker(place.latitude, place.longitude, place.nameRUS, place.id);
                    break;
                case 'cn' :
                    addMarker(place.latitude, place.longitude, place.nameCHN, place.id);
                    break;
            }
        }
    })
}

function addMarker(lat, lng, name, placeId) {
    var marker = new google.maps.Marker({
        position : {lat : lat, lng : lng},
        map : myMap
    });

    var infoWindow = new google.maps.InfoWindow({
        content : '<a style="cursor: pointer" onclick="showPlaceDetails(' + placeId + ')">' + name + '</a>'
    });

    marker.addListener('click', function () {
        infoWindow.open(myMap, marker)
    })
}