function showChart() {
    document.getElementById('order-tickets').style.display = 'none';
    document.getElementById('order-history').style.display = 'none';
    var text = loadJSON('order/get/chart');
    var toclean = document.getElementById('order-details');
    while (toclean.firstChild){
        toclean.removeChild(toclean.firstChild);
    }
    var h4 = document.getElementById('h4-price');
    if (h4)
        h4.remove();
    var button = document.getElementById('buy-button');
    if (button)
        button.remove();

    document.getElementById('places').style.display = 'none';
    document.getElementById('placeinfo').style.display = 'none';
    document.getElementById('chart').style.display = 'block';
    if (text == '[]' || !text) {
        var message = document.getElementById('chart').appendChild(document.createElement('h3'));
        message.id = 'h4-price';
        if (!lang) {
            message.innerText = 'Your cart is empty.';
        }
        else {
            switch (lang){
                case 'en' :
                    message.innerText = 'Your cart is empty.';
                    break;
                case 'ru' :
                    message.innerText = 'Ваша корзина пуста';
                    break;
                case 'cn' :
                    message.innerText = '您的购物车是空的。';
                    break;
            }
        }
    }
    else {
        var orders = JSON.parse(text);
        document.getElementById('chart-table').style.display = 'block';
        var totalPrice = 0;
        orders.forEach(function (item, i, arr) {
            var tr = document.getElementById('order-details').appendChild(document.createElement('tr'));
            var place = tr.appendChild(document.createElement('th'));
            var description = tr.appendChild(document.createElement('th'));
            var quantity = tr.appendChild(document.createElement('th'));
            var price = tr.appendChild(document.createElement('th'));
            var deleteButton = tr.appendChild(document.createElement('button'));
            deleteButton.type = 'button';
            deleteButton.className = 'btn btn-danger btn-sm';
            deleteButton.onclick = function (ev) { deleteTicket(item[8]) };

            if (!lang) {
                deleteButton.innerHTML = 'Delete';
                place.innerText = item[1];
                description.innerText = item[4];
                quantity.innerText = item[6];
                price.innerText = item[7];
                totalPrice += item[7];
            }
            else {
                switch (lang){
                    case 'en' :
                        deleteButton.innerHTML = 'Delete';
                        place.innerText = item[1];
                        description.innerText = item[4];
                        quantity.innerText = item[6];
                        price.innerText = item[7];
                        totalPrice += item[7];
                        break;
                    case 'ru' :
                        deleteButton.innerHTML = 'Удалить';
                        place.innerText = item[0];
                        description.innerText = item[3];
                        quantity.innerText = item[6];
                        price.innerText = item[7];
                        totalPrice += item[7];
                        break;
                    case 'cn' :
                        deleteButton.innerHTML = '删除';
                        place.innerText = item[2];
                        description.innerText = item[5];
                        quantity.innerText = item[6];
                        price.innerText = item[7];
                        totalPrice += item[7];
                        break;
                }
            }
        });

        var price = document.getElementById('chart').appendChild(document.createElement('h4'));
        price.id = 'h4-price';
        var buyButton = document.getElementById('chart').appendChild(document.createElement('button'));

        buyButton.type = 'button';
        buyButton.className = 'btn btn-primary btn-sm';
        buyButton.id = 'buy-button';
        buyButton.onclick = function (ev) { pay() };

        if (!lang) {
            buyButton.innerHTML = 'Buy';
            price.innerText = 'Total price: ' + totalPrice;
        }
        else {
            switch (lang){
                case 'en' :
                    buyButton.innerHTML = 'Buy';
                    price.innerText = 'Total price: ' + totalPrice;
                    break;
                case 'ru' :
                    buyButton.innerHTML = 'Купить';
                    price.innerText = 'Сумма: ' + totalPrice;
                    break;
                case 'cn' :
                    buyButton.innerHTML = 'Buy';
                    price.innerText = '总价： ' + totalPrice;
                    break;
            }
        }
    }
}

function addToChart(tickettemplateid) {
    $.get('ticket/add/'+tickettemplateid).done(function(data, textStatus, jqXHR) {
        $('#chart-info').popover('show');
        setTimeout(function () {
            $('#chart-info').popover('hide');
        }, 3000);
    }).fail(function(jqXHR, textStatus, errorThrown) {
        showLogInForm();
    });
}

function deleteTicket(tickettemplateid) {
    loadJSON('ticket/delete/'+tickettemplateid);
    showChart();
}

function pay() {
    $.get('order/pay').done(function(data, textStatus, jqXHR) {
        $('#orders-info').popover('show');
        document.getElementById('chart').style.display = 'none';
        showAllPlaces();
        setTimeout(function () {
            $('#orders-info').popover('hide');
        }, 3000);
    }).fail(function(jqXHR, textStatus, errorThrown) {
        alert('Something wrong!')
    });
}

function showHistory() {
    document.getElementById('places').style.display = 'none';
    document.getElementById('chart').style.display = 'none';
    document.getElementById('order-tickets').style.display = 'none';
    document.getElementById('order-history').style.display = 'block';
    var toclean = document.getElementById('order-list');
    while (toclean.firstChild){
        toclean.removeChild(toclean.firstChild);
    }

    var mesg = document.getElementById('order-history-message');
    if (mesg)
        mesg.remove();

    var text = loadJSON('order/get/paid');
    if (text == '[]') {
        var message = document.getElementById('order-history').appendChild(document.createElement('h3'));
        message.id = 'order-history-message';
        if (!lang) {
            message.innerText = "You don't have any orders yet.";
        }
        else {
            switch (lang){
                case 'en' :
                    message.innerText = "You don't have any orders yet.";
                    break;
                case 'ru' :
                    message.innerText = 'У Вас еще нет ни одного оплаченного заказа';
                    break;
                case 'cn' :
                    message.innerText = '你还没有任何订单。';
                    break;
            }
        }
    }
    else {
        var orders = JSON.parse(text);
        document.getElementById('order-history-table').style.display = 'block';
        orders.forEach(function (item, i, arr) {
            var tr = document.getElementById('order-list').appendChild(document.createElement('tr'));
            var date = tr.appendChild(document.createElement('th'));
            var number = tr.appendChild(document.createElement('th'));
            var price = tr.appendChild(document.createElement('th'));
            var button = tr.appendChild(document.createElement('button'));

            button.type = 'button';
            button.className = 'btn btn-primary btn-sm';
            button.id = 'show-order-button';
            button.onclick = function (ev) { showOrderDetails(item.id) };
            number.innerText = item.id;
            price.innerText = item.totalPrice;
            date.innerText = item.date;

            if (!lang) {
                button.innerHTML = 'Details';
            }
            else {
                switch (lang){
                    case 'en' :
                        button.innerHTML = 'Details';
                        break;
                    case 'ru' :
                        button.innerHTML = 'Подробнее';
                        break;
                    case 'cn' :
                        button.innerHTML = '细节';
                        break;
                }
            }
        });
    }
}

function showOrderDetails(orderID) {
    document.getElementById('order-tickets').style.display = 'block';
    document.getElementById('order-history').style.display = 'none';


    var toclean = document.getElementById('order-tickets-details');
    while (toclean.firstChild){
        toclean.removeChild(toclean.firstChild);
    }
    var mesg = document.getElementById('h4-price-order-tickets');
    if (mesg)
        mesg.remove();

    var text = loadJSON('order/get/' + orderID);
    var tickets = JSON.parse(text);

    var totalPrice = 0;
    tickets.forEach(function (item, i, arr) {
        var tr = document.getElementById('order-tickets-details').appendChild(document.createElement('tr'));
        var place = tr.appendChild(document.createElement('th'));
        var description = tr.appendChild(document.createElement('th'));
        var quantity = tr.appendChild(document.createElement('th'));
        var price = tr.appendChild(document.createElement('th'));

        quantity.innerText = item[6];
        price.innerText = item[7];
        totalPrice += item[7];

        if (!lang) {
            place.innerText = item[1];
            description.innerText = item[4];
        }
        else {
            switch (lang){
                case 'en' :
                    place.innerText = item[1];
                    description.innerText = item[4];
                    break;
                case 'ru' :
                    place.innerText = item[0];
                    description.innerText = item[3];
                    break;
                case 'cn' :
                    place.innerText = item[2];
                    description.innerText = item[5];
                    break;
            }
        }
    });

    var price = document.getElementById('order-tickets').appendChild(document.createElement('h4'));
    price.id = 'h4-price-order-tickets';

    if (!lang) {
        price.innerText = 'Total price: ' + totalPrice;
    }
    else {
        switch (lang){
            case 'en' :
                price.innerText = 'Total price: ' + totalPrice;
                break;
            case 'ru' :
                price.innerText = 'Сумма: ' + totalPrice;
                break;
            case 'cn' :
                price.innerText = '总价： ' + totalPrice;
                break;
        }
    }
}
