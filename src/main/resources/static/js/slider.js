var idx = 0;

function moveLeft(){
    var slide_left = document.getElementById('slide-left');
    var slide_right = document.getElementById('slide-right');
    var total_img = document.getElementsByClassName('slide');

    slide_left.style.display = 'block';
    total_img[idx].style.display = 'none';
    total_img[++idx].style.display = 'block';
    if (idx === total_img.length - 1) {
        slide_right.style.display = 'none';
    }
}

function moveRight(){
    var slide_left = document.getElementById('slide-left');
    var slide_right = document.getElementById('slide-right');
    var total_img = document.getElementsByClassName('slide');

    slide_right.style.display = 'block';
    total_img[idx].style.display = 'none';
    total_img[--idx].style.display = 'block';
    if (idx === 0) {
        slide_left.style.display = 'none';
    }
}