$(function() {

    $("form#data").submit(function(event){

        event.preventDefault();

        var url  = '/image/add/' + document.getElementById('img-upload-place-id').innerText;
        var image_file = $('#image_file').get(0).files[0];

        var formData = new FormData();
        formData.append("file", image_file);

        $.ajax({
            url: url,
            type: 'POST',
            data: formData,
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            success: function (status) {
                // do something on success
            }
        });

        return false;

    });

});

function deleteImage(name, id) {
    $.get('image/delete/' + name).done(function(data, textStatus, jqXHR) {
        showPlaceDetails(id)
    }).fail(function(jqXHR, textStatus, errorThrown) {
        alert('Cannot delete image')
    });
}