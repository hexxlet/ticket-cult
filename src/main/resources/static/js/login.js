function showLogInForm() {
    $('#loginModal').modal();
}

var loginpopover = false;
var logoutpopup = false;
var signinpopover = false;


function login() {
    var data = 'username=' + $('#username').val() + '&password=' + $('#password').val();
    $.ajax({
        data: data,
        timeout: 1000,
        type: 'POST',
        url: '/login'

    }).done(function(data, textStatus, jqXHR) {
        loginpopover = true;
        location.reload();
    }).fail(function(jqXHR, textStatus, errorThrown) {
        if (!lang) {
            alert('Booh! Wrong credentials, try again!');
        }
        else {
            switch (lang){
                case 'en' :
                    alert('Booh! Wrong credentials, try again!');
                    break;
                case 'ru' :
                    alert('Ууупс - неверные данные');
                    break;
                case 'cn' :
                    alert('布布！ 错误的凭据，请重试！');
                    break;
            }
        }
    });
}

function loadSignUpForm() {
    $('#signUpModal').modal();
}

function signUp() {
    var user = {};
    user.name = $('#firstname-sign-up').val();
    user.lastname = $('#lastname-sign-up').val();
    user.email = $('#email-sign-up').val();
    user.role= $('input[name=roleradio]:checked').val();
    user.passwordHash = $('#password-sign-up').val();

    $.ajax({
        data: JSON.stringify(user),
        timeout: 1000,
        type: 'POST',
        url: 'user/createorupdate',
        contentType: "application/json; charset=utf-8",
        dataType: "json"
    }).done(function(data, textStatus, jqXHR) {
        signinpopover = true;
        location.reload();
    }).fail(function(jqXHR, textStatus, errorThrown) {
        if (!lang) {
            alert('Cannot sign up');
        }
        else {
            switch (lang){
                case 'en' :
                    alert('Cannot sign up');
                    break;
                case 'ru' :
                    alert('Не получается зарегистрироваться');
                    break;
                case 'cn' :
                    alert('无法注册');
                    break;
            }
        }
    });
}

function logout(){
    logoutpopup = true;
    window.location.replace("/logout");
}

$( function () {
    if ( signinpopover ) {
        $('#success-sign-up').popover('show');
        signinpopover = false;
        setTimeout(function () {
            $('#success-sign-up').popover('hide');
        }, 3000);
    }

    if ( loginpopover ) {
        $('#success-log-in').popover('show');
        loginpopover = false;
        setTimeout(function () {
            $('#success-log-in').popover('hide');
        }, 3000);
    }

    if ( logoutpopup ) {
        $('#success-log-out').popover('show');
        logoutpopup = false;
        setTimeout(function () {
            $('#success-log-out').popover('hide');
        }, 3000);
    }
} );