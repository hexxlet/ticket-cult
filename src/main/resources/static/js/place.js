var lang;
function onLoad() {
    lang = sessionStorage.getItem('lang');
    if (lang){
        translate();
    }
    if ($('#role').val() == 'PLACEOWNER'){
        showAllPlacesOfPlaceOwner();
    }
    else showAllPlaces();
}

function loadJSON(url) {
    var xhr = new XMLHttpRequest();

    xhr.open('GET', url, false);
    xhr.send();

    if (xhr.status != 200) {
        if (xhr.status == 401){
            showLogInForm();
        }
        else{
            alert('Ошибка ' + xhr.status + ': ' + xhr.statusText);
        }
    } else {
        // вывести результат
        return xhr.responseText;
    }
}

function showAllPlaces() {
    var text = loadJSON('place/get/all');
    var places = JSON.parse(text);
    var toclean = document.getElementById('log');
    while (toclean.firstChild) {
        toclean.removeChild(toclean.firstChild);
    }

    places.forEach(function (item, i, arr) {
        var tr = log.appendChild(document.createElement('tr'));
        var trName = tr.appendChild(document.createElement('th'));
        var trHours = tr.appendChild(document.createElement('th'));
        var trDesciprion = tr.appendChild(document.createElement('th'));
        var button = tr.appendChild(document.createElement('button'));

        var id = item.id;

        if (!lang) {
            trDesciprion.innerHTML = item.descriptionENG;
            trName.innerHTML = item.nameENG;
            trHours.innerHTML = item.openingHoursENG;
            button.innerText = 'Info';
        }
        else {
            switch (lang){
                case 'en' :
                    trName.innerHTML = item.nameENG;
                    trHours.innerHTML = item.openingHoursENG;
                    trDesciprion.innerHTML = item.descriptionENG;
                    button.innerText = 'Info';
                    break;
                case 'ru' :
                    trDesciprion.innerHTML = item.descriptionRUS;
                    trName.innerHTML = item.nameRUS;
                    trHours.innerHTML = item.openingHoursRUS;
                    button.innerText = 'Подробнее';
                    break;
                case 'cn' :
                    trDesciprion.innerHTML = item.descriptionCHN;
                    trName.innerHTML = item.nameCHN;
                    trHours.innerHTML = item.openingHoursCHN;
                    button.innerText = '信息';
                    break;
            }
        }
        button.type = 'button';
        button.className = 'btn btn-primary btn-sm';
        button.onclick = function (ev) { showPlaceDetails(id) };
    });
    document.getElementById('places').style.display = 'block';
}


function showAllPlacesOfPlaceOwner() {
    document.getElementById('add-place').style.display = 'none';
    document.getElementById('ownerplaces').style.display = 'block';
    document.getElementById('map').style.display = 'block';

    var text = loadJSON('place/get/byuserid');
    var places = JSON.parse(text);
    var toclean = document.getElementById('log');
    while (toclean.firstChild) {
        toclean.removeChild(toclean.firstChild);
    }

    places.forEach(function (item, i, arr) {
        var tr = log.appendChild(document.createElement('tr'));
        var trName = tr.appendChild(document.createElement('th'));
        var trHours = tr.appendChild(document.createElement('th'));
        var trDesciprion = tr.appendChild(document.createElement('th'));
        var button = tr.appendChild(document.createElement('button'));
        var updatePlace = tr.appendChild(document.createElement('button'));
        var deletePlace = tr.appendChild(document.createElement('button'));

        var id = item.id;
        button.type = 'button';
        button.className = 'btn btn-primary btn-sm special';
        button.style.marginBottom = '5px';
        button.style.marginTop = '10px';
        button.onclick = function (ev) { showPlaceDetails(id) };
        updatePlace.type = 'button';
        updatePlace.style.marginBottom = '5px';
        updatePlace.className = 'btn btn-primary btn-sm special';
        updatePlace.onclick = function (ev) { loadUpdatePlaceForm(id) };
        deletePlace.type = 'button';
        deletePlace.style.marginBottom = '10px';
        deletePlace.className = 'btn btn-danger btn-sm special';
        deletePlace.onclick = function (ev) { deletePlace(id) };

        if (!lang) {
            trName.innerHTML = item.nameENG;
            trHours.innerHTML = item.openingHoursENG;
            trDesciprion.innerHTML = item.descriptionENG;
            button.innerText = 'Info';
            updatePlace.innerText = 'Update';
            deletePlace.innerText = 'Delete';
        }
        else {
            switch (lang){
                case 'en' :
                    trName.innerHTML = item.nameENG;
                    trHours.innerHTML = item.openingHoursENG;
                    trDesciprion.innerHTML = item.descriptionENG;
                    button.innerText = 'Info';
                    updatePlace.innerText = 'Update';
                    deletePlace.innerText = 'Delete';
                    break;
                case 'ru' :
                    trName.innerHTML = item.nameRUS;
                    trHours.innerHTML = item.openingHoursRUS;
                    trDesciprion.innerHTML = item.descriptionRUS;
                    button.innerText = 'Информация';
                    updatePlace.innerText = 'Обновить';
                    deletePlace.innerText = 'Удалить';
                    break;
                case 'cn' :
                    trName.innerHTML = item.nameCHN;
                    trHours.innerHTML = item.openingHoursCHN;
                    trDesciprion.innerHTML = item.descriptionCHN;
                    button.innerText = '信息';
                    updatePlace.innerText = '更新';
                    deletePlace.innerText = '删除';
                    break;
            }
        }
    });
    document.getElementById('places').style.display = 'block';
}

function showPlaceDetails(id) {
    document.getElementById('map').style.display = 'none';
    document.getElementById('places').style.display = 'none';
    var div = document.getElementById('placeinfo');
    while (div.firstChild) {
        div.removeChild(div.firstChild);
    }
    div.style.display = 'block';
    if (document.getElementById('role')) {
        if (document.getElementById('role').value == 'PLACEOWNER')
            document.getElementById('add-template').style.display = 'block';
    }

    var text = loadJSON('place/get/byplaceid/'+id);
    var place = JSON.parse(text);

    var name = document.getElementById('placeinfo').appendChild(document.createElement('h1'));
    document.getElementById('placeinfo').appendChild(document.createElement('br'));
    var description = document.getElementById('placeinfo').appendChild(document.createElement('p'));
    document.getElementById('placeinfo').appendChild(document.createElement('br'));


    if (!lang) {
        name.innerText = place.nameENG;
        description.innerHTML = place.descriptionENG;
    }
    else {
        switch (lang){
            case 'en' :
                name.innerText = place.nameENG;
                description.innerHTML = place.descriptionENG;
                break;
            case 'ru' :
                name.innerText = place.nameRUS;
                description.innerHTML = place.descriptionRUS;
                break;
            case 'cn' :
                name.innerText = place.nameCHN;
                description.innerHTML = place.descriptionCHN;
                break;
        }
    }

    var hidden = div.appendChild(document.createElement('p'));
    hidden.id = 'hidden';
    hidden.style.display = 'none';
    hidden.innerText = id;

    var ticketsTable = div.appendChild(document.createElement('table'));
    ticketsTable.id = 'ticketsTable';
    ticketsTable.style.borderCollapse = 'inherit';
    ticketsTable.style.borderSpacing = '5px';
    var ticketDescJSON = loadJSON('/tickettemplate/get/byplaceid/'+id);
    var ticketTemplates = JSON.parse(ticketDescJSON);

    ticketTemplates.forEach(function (item, i, arr) {
        var row = ticketsTable.appendChild(document.createElement('tr'));
        var description = row.appendChild(document.createElement('td'));
        var price = row.appendChild(document.createElement('td'));
        var currency = row.appendChild(document.createElement('td'));

        if ($('#role').val() == 'PLACEOWNER') {
            var updateT = row.appendChild(document.createElement('button'));
            var deleteT = row.appendChild(document.createElement('button'));
            row.appendChild(document.createElement('br'));

            updateT.type = 'button';
            updateT.className = 'btn btn-primary btn-sm';
            updateT.onclick = function (ev) { loadTemplateUpdateForm(item.id) };
            deleteT.type = 'button';
            deleteT.className = 'btn btn-danger btn-sm';
            deleteT.onclick = function (ev) { deleteTicketTemplate(item.id) };

            if (!lang) {
                updateT.innerText = 'Update';
                deleteT.innerText = 'Delete';
            }
            else {
                switch (lang){
                    case 'en' :
                        updateT.innerText = 'Update';
                        deleteT.innerText = 'Delete';
                        break;
                    case 'ru' :
                        updateT.innerText = 'Обновить';
                        deleteT.innerText = 'Удалить';
                        break;
                    case 'cn' :
                        updateT.innerText = '更新';
                        deleteT.innerText = '删除';
                        break;
                }
            }
        }
        if (($('#role').val() == 'USER') || ($('#isAuthorized').val() == 'false')) {
            var button = row.appendChild(document.createElement('button'));
            row.appendChild(document.createElement('br'));

            button.type = 'button';
            button.className = 'btn btn-primary btn-sm';
            button.onclick = function (ev) { addToChart(item.id) };

            if (!lang) {
                button.innerText = 'Add to cart';
            }
            else {
                switch (lang){
                    case 'en' :
                        button.innerText = 'Add to chart';
                        break;
                    case 'ru' :
                        button.innerText = 'Добавить в корзину';
                        break;
                    case 'cn' :
                        button.innerText = '添加到购物车';
                        break;
                }
            }
        }

        price.innerHTML = item.price;
        currency.innerHTML = item.currency;
        if (!lang) {
            description.innerHTML = item.descriptionENG;
        }
        else {
            switch (lang){
                case 'en' :
                    description.innerHTML = item.descriptionENG;
                    break;
                case 'ru' :
                    description.innerHTML = item.descriptionRUS;
                    break;
                case 'cn' :
                    description.innerHTML = item.descriptionCHN;
                    break;
            }
        }
    });

    document.getElementById('img-upload-place-id').innerText = id;

    if (($('#role').val() == 'USER') || ($('#isAuthorized').val() == 'false')) {
        document.getElementById('img-upload').style.display = 'none';
    }
    else {
        if ($('#role').val() == 'PLACEOWNER') {
            document.getElementById('img-upload').style.display = 'block';
        }
    }

    var text = loadJSON('image/get/names/' + document.getElementById('img-upload-place-id').innerText);
    var imageNames = JSON.parse(text);

    var sliderDiv = document.getElementById('slider-div');
    var content = document.getElementById('slider-content');
    sliderDiv.style.display = 'block';

    imageNames.forEach(function (item) {
        var a = content.appendChild(document.createElement('a'));
        a.style.cursor = 'pointer';
        a.onclick = function (ev) { deleteImage(item, id) };
        var img = a.appendChild(document.createElement('img'));
        img.className = 'slide img-thumbnail';
        img.src = 'place-img/' + item;
    })




}

function deletePlace(id) {
    loadJSON('place/delete/'+id);
    showAllPlacesOfPlaceOwner();
}

function loadUpdatePlaceForm(id) {
    document.getElementById('places').style.display = 'none';
    document.getElementById('place-update').style.display = 'block';
    var text = loadJSON('place/get/byplaceid/'+id);
    var place = JSON.parse(text);
    document.getElementById('form-place-id').innerText = place.id;
    document.getElementById('nameArea').innerText = place.nameENG;
    document.getElementById('nameAreaRUS').innerText = place.nameRUS;
    document.getElementById('nameAreaCHN').innerText = place.nameCHN;

    document.getElementById('hoursArea').innerText = place.openingHoursENG;
    document.getElementById('hoursAreaRUS').innerText = place.openingHoursRUS;
    document.getElementById('hoursAreaCHN').innerText = place.openingHoursCHN;

    document.getElementById('descriptionArea').innerText = place.descriptionENG;
    document.getElementById('descriptionAreaRUS').innerText = place.descriptionENG;
    document.getElementById('descriptionAreaCHN').innerText = place.descriptionENG;

    document.getElementById('update-latitude').innerText = place.latitude;
    document.getElementById('update-longitude').innerText = place.longitude;
}

function updatePlace() {
    var id = document.getElementById('form-place-id').value;
    var text = loadJSON('place/get/byplaceid/'+id);
    var place = JSON.parse(text);

    place.nameENG = document.getElementById('nameArea').value;
    place.nameRUS = document.getElementById('nameAreaRUS').value;
    place.nameCHN = document.getElementById('nameAreaCHN').value;

    place.openingHoursENG = document.getElementById('hoursArea').value;
    place.openingHoursRUS = document.getElementById('hoursAreaRUS').value;
    place.openingHoursCHN = document.getElementById('hoursAreaCHN').value;

    place.descriptionENG = document.getElementById('descriptionArea').value;
    place.descriptionRUS = document.getElementById('descriptionAreaRUS').value;
    place.descriptionCHN = document.getElementById('descriptionAreaCHN').value;

    place.latitude = document.getElementById('update-latitude').value;
    place.longitude = document.getElementById('update-longitude').value;

    $.ajax({
        type: "POST",
        url: "place/createorupdate",
        // The key needs to match your method's input parameter (case-sensitive).
        data: JSON.stringify(place),
        contentType: "application/json; charset=utf-8",
        dataType: "json"
    }).done(function (data, textStatus, jqXHR) {
        document.getElementById('place-update').style.display = 'none';
        showAllPlacesOfPlaceOwner();
        document.getElementById('places').style.display = 'block';
        initMap();
    }).fail(function (jqXHR, textStatus, errorThrown) {
        if (jqXHR.status === 401) { // HTTP Status 401: Unauthorized
            showLogInForm();
        } else {
            alert('Houston, we have a problem...');
        }
    });
}

function addNewPlace() {
    var place = {};
    place.nameENG = $('#newNameArea').val();
    place.nameRUS = $('#newNameAreaRUS').val();
    place.nameCHN = $('#newNameAreaCHN').val();

    place.openingHoursENG = $('#newHoursArea').val();
    place.openingHoursRUS = $('#newHoursAreaRUS').val();
    place.openingHoursCHN = $('#newHoursAreaCHN').val();

    place.descriptionRUS = $('#RusDescriptionArea').val();
    place.descriptionENG = $('#EngDescriptionArea').val();
    place.descriptionCHN = $('#CnhDescriptionArea').val();
    place.latitude = $('#add-latitude').val();
    place.longitude = $('#add-longitude').val();

    $.ajax({
        data: JSON.stringify(place),
        timeout: 1000,
        type: 'POST',
        url: 'place/createorupdate',
        contentType: "application/json; charset=utf-8",
        dataType: "json"
    }).done(function(data, textStatus, jqXHR) {
        showAllPlacesOfPlaceOwner();
        var form = document.getElementById('add-place-form');
        form.reset();
        initMap();
    }).fail(function(jqXHR, textStatus, errorThrown) {
        alert('Cannot add place');
    });
}

function deleteTicketTemplate(ticketTemplateID, placeid) {
    loadJSON('tickettemplate/delete/' + ticketTemplateID);
    showPlaceDetails(placeid);
}

function loadTemplateUpdateForm(ticketTemplateID) {
    document.getElementById('add-template').style.display = 'none';
    document.getElementById('placeinfo').style.display = 'none';
    document.getElementById('template-update').style.display = 'block';
    var text = loadJSON('tickettemplate/get/byid/'+ticketTemplateID);
    var template = JSON.parse(text);
    document.getElementById('form-template-id').innerText = ticketTemplateID;

    document.getElementById('templateDescriptionENG').innerText = template.descriptionENG;
    document.getElementById('templateDescriptionRUS').innerText = template.descriptionRUS;
    document.getElementById('templateDescriptionCNH').innerText = template.descriptionCHN;
    document.getElementById('templatePrice').innerText = template.price;
}

function updateTemplate() {
    var id = document.getElementById('form-template-id').value;
    var text = loadJSON('tickettemplate/get/byid/'+id);
    var template = JSON.parse(text);
    template.descriptionENG = document.getElementById('templateDescriptionENG').value;
    template.descriptionRUS = document.getElementById('templateDescriptionRUS').value;
    template.descriptionCHN = document.getElementById('templateDescriptionCNH').value;
    template.price = document.getElementById('templatePrice').value;
    var hidden = document.getElementById('hidden').innerText;

    $.ajax({
        type: "POST",
        url: "tickettemplate/createorupdate/" + hidden,
        // The key needs to match your method's input parameter (case-sensitive).
        data: JSON.stringify(template),
        contentType: "application/json; charset=utf-8",
        dataType: "json"
    }).done(function (data, textStatus, jqXHR) {
        document.getElementById('template-update').style.display = 'none';
        showPlaceDetails(template.place.id);
    }).fail(function (jqXHR, textStatus, errorThrown) {
        if (jqXHR.status === 401) { // HTTP Status 401: Unauthorized
            showLogInForm();
        } else {
            alert('Houston, we have a problem...');
        }
    });
}

function addNewTemplate() {
    var template = {};
    template.descriptionENG = $('#utemplateDescriptionENG').val();
    template.descriptionRUS = $('#utemplateDescriptionRUS').val();
    template.descriptionCHN = $('#utemplateDescriptionCNH').val();
    template.price = $('#utemplatePrice').val();
    var hidden = document.getElementById('hidden').innerText;

    $.ajax({
        data: JSON.stringify(template),
        timeout: 1000,
        type: 'POST',
        url: 'tickettemplate/createorupdate/' + hidden,
        contentType: "application/json; charset=utf-8",
        dataType: "json"
    }).done(function(data, textStatus, jqXHR) {
        document.getElementById('add-template-form').reset();
        showPlaceDetails(hidden);

    }).fail(function(jqXHR, textStatus, errorThrown) {
        alert('Cannot add template');
    });
}

function showAddPlaceForm() {
    document.getElementById('add-place').style.display = 'block';
    document.getElementById('ownerplaces').style.display = 'none';
    document.getElementById('map').style.display = 'none';
}