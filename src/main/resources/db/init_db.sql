﻿-- Exported from QuickDBD: https://www.quickdatatabasediagrams.com/
-- Link to schema: https://app.quickdatabasediagrams.com/#/schema/g-fVmvGOM0-DY1rhzYs7Rg
-- NOTE! If you have used non-SQL datatypes in your design, you will have to change these here.


CREATE TABLE "system_user" (
    "id"  SERIAL  NOT NULL,
    "name" varchar(15)   NOT NULL,
    "lastname" varchar(15)   NOT NULL,
    "email" varchar(30)   NOT NULL,
    "language" varchar(15)   NOT NULL,
    "hashed_password" text   NOT NULL,
    "role" varchar(15)   NOT NULL,
    "is_disabled" boolean   NOT NULL,
    CONSTRAINT "pk_system_user" PRIMARY KEY (
        "id"
     ),
    CONSTRAINT "uc_system_user_email" UNIQUE (
        "email"
    )
);

CREATE TABLE "system_order" (
    "id"  SERIAL  NOT NULL,
    "user_id" int   NOT NULL,
    "date" timestamp   NOT NULL,
    "total_price" decimal   NOT NULL,
    "currency" varchar(15)   NOT NULL,
    "status" varchar(15)   NOT NULL,
    CONSTRAINT "pk_order" PRIMARY KEY (
        "id"
     )
);

CREATE TABLE "ticket_template" (
    "id"  SERIAL  NOT NULL,
    "place_id" int   NOT NULL,
    "price" decimal   NOT NULL,
    "currency" varchar(15)   NOT NULL,
    "description_rus" text   NOT NULL,
    "description_eng" text   NOT NULL,
    "description_chn" text   NOT NULL,
    "is_enabled" boolean,
    CONSTRAINT "pk_ticket_template" PRIMARY KEY (
        "id"
     )
);

CREATE TABLE "place" (
    "id"  SERIAL  NOT NULL,
    "name_rus" varchar(50)   NOT NULL,
    "name_eng" varchar(50)   NOT NULL,
    "name_chn" varchar(50)   NOT NULL,
    "owner_id" int   NOT NULL,
    "opening_hours_rus" text   NOT NULL,
    "opening_hours_eng" text   NOT NULL,
    "opening_hours_chn" text   NOT NULL,
    "description_rus" text   NOT NULL,
    "description_eng" text   NOT NULL,
    "description_chn" text   NOT NULL,
    "latitude" float8 NOT NULL,
    "longitude" float8 NOT NULL,
    CONSTRAINT "pk_place" PRIMARY KEY (
        "id"
     )
);

CREATE TABLE "ticket" (
    "id"  SERIAL  NOT NULL,
    "ticket_template_id" int   NOT NULL,
    "order_id" int   NOT NULL,
    CONSTRAINT "pk_ticket" PRIMARY KEY (
        "id"
     )
);

ALTER TABLE "order" ADD CONSTRAINT "fk_order_user_id" FOREIGN KEY("user_id")
REFERENCES "system_user" ("id");

ALTER TABLE "ticket_template" ADD CONSTRAINT "fk_ticket_template_place_id" FOREIGN KEY("place_id")
REFERENCES "place" ("id") ON DELETE CASCADE;

ALTER TABLE "place" ADD CONSTRAINT "fk_place_owner_id" FOREIGN KEY("owner_id")
REFERENCES "system_user" ("id");

ALTER TABLE "ticket" ADD CONSTRAINT "fk_ticket_ticket_template_id" FOREIGN KEY("ticket_template_id")
REFERENCES "ticket_template" ("id");

ALTER TABLE "ticket" ADD CONSTRAINT "fk_ticket_order_id" FOREIGN KEY("order_id")
REFERENCES "order" ("id");

