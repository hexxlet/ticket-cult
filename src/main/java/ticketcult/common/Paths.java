package ticketcult.common;

public class Paths {

    public final static String TICKET_TEMPLATE = "/tickettemplate";
    public final static String PLACE = "/place";
    public final static String USER = "/user";
    public final static String TICKET = "/ticket";
    public final static String ORDER = "/order";
    public static final String IMAGE = "/image";
}
