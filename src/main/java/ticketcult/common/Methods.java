package ticketcult.common;

public class Methods {

    public final static String CREATE_OR_UPDATE = "/createorupdate";
    public final static String GET = "/get";
    public final static String DELETE = "/delete";
    public final static String ADD = "/add";
    public static final String PAY = "/pay";
}
