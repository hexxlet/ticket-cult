package ticketcult.controller;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ticketcult.common.Methods;
import ticketcult.common.Paths;
import ticketcult.enums.OrderStatus;
import ticketcult.model.Order;
import ticketcult.service.OrderService;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping(Paths.ORDER)
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class OrderController {

    OrderService orderService;

    @PreAuthorize("hasAuthority('USER')")
    @RequestMapping(Methods.GET + "/chart")
    public List<Object[]> showChart(Principal principal){
        return orderService.findUnpaidOrderDetails(principal.getName());
    }


    @PreAuthorize("hasAuthority('USER')")
    @RequestMapping(Methods.PAY)
    public void pay(Principal principal){
        orderService.pay(principal.getName());
    }

    @PreAuthorize("hasAuthority('USER')")
    @RequestMapping(Methods.GET + "/paid")
    public List<Order> getPaidOrders(Principal principal){
        return orderService.findOrdersByStatus(OrderStatus.PAID, principal.getName());
    }

    @PreAuthorize("hasAuthority('USER')")
    @RequestMapping(Methods.GET + "/{id}")
    public List<Object[]> showOrderDatails(@PathVariable final int id, Principal principal){
        return orderService.findOrderDetails(id);
    }
}
