package ticketcult.controller;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ticketcult.common.Methods;
import ticketcult.common.Paths;
import ticketcult.model.TicketTemplate;
import ticketcult.repo.TicketTemplateRepository;
import ticketcult.service.TicketTemplateService;

import java.security.Principal;
import java.util.Optional;

@RestController
@RequestMapping(Paths.TICKET_TEMPLATE)
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class TicketTemplateController {

    TicketTemplateRepository ticketTemplateRepository;
    TicketTemplateService templateService;

    @PostMapping(Methods.CREATE_OR_UPDATE + "/{placeID}")
    public TicketTemplate createTicketTeplate(@RequestBody final TicketTemplate template,
                                              @PathVariable final int placeID){
        return templateService.save(template, placeID);
    }

    @GetMapping(Methods.GET + "/byplaceid/{placeID}")
    public Iterable<TicketTemplate> getByPlaceID(@PathVariable final int placeID){
        return ticketTemplateRepository.findAllByPlaceIdAndEnabled(placeID, true);
    }

    @GetMapping(Methods.GET + "/byid/{id}")
    public TicketTemplate getByID(@PathVariable final int id){
        Optional<TicketTemplate> template = ticketTemplateRepository.findByIdAndEnabled(id, true);
        if (template.isPresent())
            return template.get();
        else throw new RuntimeException("No such template");
    }

    @PreAuthorize("hasAuthority('PLACEOWNER')")
    @GetMapping(Methods.DELETE + "/{id}")
    public void deleteTicketTemplate(@PathVariable final int id, Principal principal){
        templateService.disableTemplate(id, principal);
    }
}
