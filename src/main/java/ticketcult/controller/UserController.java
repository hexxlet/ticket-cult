package ticketcult.controller;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ticketcult.common.Methods;
import ticketcult.common.Paths;
import ticketcult.model.User;
import ticketcult.repo.UserRepository;

import java.util.Optional;

@RestController
@RequestMapping(Paths.USER)
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class UserController {

    UserRepository userRepository;

    @GetMapping(Methods.GET + "/all")
    public Iterable<User> getAllUsers(){
        return userRepository.findAll();
    }

    @GetMapping(Methods.GET + "/{userID}")
    public User getUserByID(@PathVariable final int userID){
        Optional<User> user = userRepository.findById(userID);
        if (user.isPresent())
            return user.get();
        else throw new RuntimeException("No user found");
    }

    @PostMapping(Methods.CREATE_OR_UPDATE)
    public User createUser(@RequestBody final User user){
        User tmp = user;
        return userRepository.save(user);
    }
}
