package ticketcult.controller;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ticketcult.common.Methods;
import ticketcult.common.Paths;
import ticketcult.model.Place;
import ticketcult.service.PlaceService;

import java.security.Principal;


@RestController
@RequestMapping(Paths.PLACE)
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class PlaceController {

    PlaceService placeService;

    @GetMapping(Methods.GET + "/all")
    public Iterable<Place> getAllPlaces(){
        return placeService.findAll();
    }

    @GetMapping(Methods.GET + "/byplaceid/{placeID}")
    public Place getPlaceById(@PathVariable final int placeID){
        return placeService.findById(placeID);
    }

    @PreAuthorize("hasAnyAuthority('PLACEOWNER')")
    @GetMapping(Methods.GET + "/byuserid")
    public Iterable<Place> getAllPlacesOfUser(Principal principal){
        return placeService.findAllByUserId(principal);
    }

    @PreAuthorize("hasAnyAuthority('PLACEOWNER')")
    @PostMapping(Methods.CREATE_OR_UPDATE)
    public Place createPlace(@RequestBody final Place place, Principal principal){
        Place pls = place;
        return placeService.save(place, principal);
    }

    @PreAuthorize("hasAnyAuthority('PLACEOWNER')")
    @GetMapping(Methods.DELETE + "/{placeID}")
    public void deletePlace(@PathVariable final int placeID, Principal principal){
        placeService.deleteById(placeID, principal);
    }
}
