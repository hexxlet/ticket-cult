package ticketcult.controller;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ticketcult.common.Methods;
import ticketcult.common.Paths;
import ticketcult.service.TicketService;

import java.security.Principal;

@RestController
@AllArgsConstructor(onConstructor = @__(@Autowired))
@RequestMapping(Paths.TICKET)
public class TicketController {

    TicketService ticketService;

    @PreAuthorize("hasAuthority('USER')")
    @GetMapping(Methods.ADD + "/{ticketTemplateID}")
    public void addTicket(@PathVariable final int ticketTemplateID, Principal principal){
        ticketService.addTicketToUserOrder(ticketTemplateID, principal.getName());
    }

    @PreAuthorize("hasAuthority('USER')")
    @GetMapping(Methods.DELETE + "/{ticketTemplateID}")
    public void deleteTicketFromOrder(@PathVariable final int ticketTemplateID, Principal principal){
        ticketService.deleteFromOrder(ticketTemplateID, principal.getName());
    }
}
