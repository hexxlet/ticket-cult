package ticketcult.controller;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ticketcult.common.Methods;
import ticketcult.common.Paths;
import ticketcult.service.ImageService;

import java.io.IOException;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping(Paths.IMAGE)
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class ImageController {

    ImageService imageService;

    @PostMapping(Methods.ADD + "/{placeId}")
    @PreAuthorize("hasAuthority('PLACEOWNER')")
    public void addImage(@PathVariable int placeId, @RequestParam("file")MultipartFile file, Principal principal) throws IOException {
        imageService.createImage(file, placeId);
    }

    @GetMapping(Methods.DELETE + "/{filename}")
    @PreAuthorize("hasAuthority('PLACEOWNER')")
    public void deleteImage(@PathVariable String filename, Principal principal) throws IOException {
        imageService.deleteImage(filename);
    }

    @GetMapping(Methods.GET + "/names/{placeId}")
    public List<String> getImageNames(@PathVariable int placeId){
        return imageService.findImagesByPlace(placeId);
    }

}
