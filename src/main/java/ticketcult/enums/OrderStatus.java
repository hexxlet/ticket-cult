package ticketcult.enums;

public enum OrderStatus {
    UNPAID,
    PAID
}
