package ticketcult.service;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ticketcult.enums.Currency;
import ticketcult.exception.PlaceNotFoundException;
import ticketcult.exception.TicketTemplateException;
import ticketcult.model.Place;
import ticketcult.model.TicketTemplate;
import ticketcult.repo.PlaceRepository;
import ticketcult.repo.TicketTemplateRepository;

import java.security.Principal;
import java.util.Optional;

@Component
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class TicketTemplateService {

    TicketTemplateRepository templateRepository;
    PlaceRepository placeRepository;

    public void disableTemplate(int templateID, Principal principal) {
        Optional<TicketTemplate> templateOpt = templateRepository.findByIdAndEnabled(templateID, true);
        if (!templateOpt.isPresent())
            throw new TicketTemplateException("No such template");
        else {
            TicketTemplate template = templateOpt.get();
            template.setEnabled(false);
            templateRepository.save(template);
        }
    }

    public TicketTemplate save(TicketTemplate template, int placeID) {
        if (template.getPlace() != null)
            return templateRepository.save(template);
        else {
            Optional<Place> placeOpt = placeRepository.findById(placeID);
            if (placeOpt.isPresent()) {
                Place place = placeOpt.get();
                template.setPlace(place);
                template.setEnabled(true);
                template.setCurrency(Currency.RUB);
                return templateRepository.save(template);
            }
            else throw new PlaceNotFoundException("No such place found");
        }
    }
}
