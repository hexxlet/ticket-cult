package ticketcult.service;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ticketcult.enums.OrderStatus;
import ticketcult.exception.OrderException;
import ticketcult.model.Order;
import ticketcult.model.Ticket;
import ticketcult.repo.OrderRepository;
import ticketcult.repo.TicketRepository;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Component
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class OrderService {

    OrderRepository orderRepository;
    TicketRepository ticketRepository;

    public int calculateTotalPrice(int orderID){
        Iterable<Ticket> allTicketsOfOrder = ticketRepository.findAllByOrderId(orderID);
        int summOfOrder = 0;

        Iterator<Ticket> iterator = allTicketsOfOrder.iterator();
        while (iterator.hasNext()){
            Ticket ticket = iterator.next();
            summOfOrder += ticket.getTicketTemplate().getPrice();
        }
        return summOfOrder;
    }


    public List<Order> findUserOrders(String userEmail) {
        List<Order> orders = new ArrayList<>();
        orderRepository.findAllByUserEmail(userEmail).forEach(orders::add);
        return orders;
    }

    public void createOrUpdate(Order order) {
        orderRepository.save(order);
    }

    public List<Order> findOrdersByStatus(OrderStatus status, String email){
        List<Order> orders = new ArrayList<>();
        orderRepository.findAllByStatusAndUserEmail(status, email).forEach(orders::add);
        return orders;
    }

    public List<Object[]> findUnpaidOrderDetails(String email) {
        List<Order> ordersByStatus = findOrdersByStatus(OrderStatus.UNPAID, email);
        if (!ordersByStatus.isEmpty()) {
            int orderID = ordersByStatus.get(0).getId();
            return orderRepository.findDeatailedOrderInfo(orderID);
        }
        else return null;
    }

    public void pay(String email) {
        List<Order> unpaidOrders = findOrdersByStatus(OrderStatus.UNPAID, email);
        if (unpaidOrders.isEmpty())
            throw new OrderException("No unpaid orders");
        else {
            Order order = unpaidOrders.get(0);
            order.setStatus(OrderStatus.PAID);
            orderRepository.save(order);
        }
    }

    public List<Object[]> findOrderDetails(int orderID) {
        return orderRepository.findDeatailedOrderInfo(orderID);
    }
}
