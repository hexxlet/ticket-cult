package ticketcult.service;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ticketcult.exception.AccessException;
import ticketcult.exception.PlaceNotFoundException;
import ticketcult.model.Place;
import ticketcult.model.User;
import ticketcult.repo.PlaceRepository;
import ticketcult.repo.UserRepository;

import java.security.Principal;
import java.util.Optional;

@Component
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class PlaceService {

    PlaceRepository placeRepository;
    UserRepository userRepository;

    public Place save(Place place, Principal principal) {
        if (place.getUser() != null)
            return placeRepository.save(place);
        else {
            User retrievedUser = userRepository.findByEmail(principal.getName());
            place.setUser(retrievedUser);
            return placeRepository.save(place);
        }
    }

    public Iterable<Place> findAll() {
        Iterable<Place> allPlaces = placeRepository.findAll();
        allPlaces.forEach(place -> place.setUser(null));
        return allPlaces;
    }


    public Place findById(int placeID) {
        Optional<Place> optionalPlace = placeRepository.findById(placeID);
        if (optionalPlace.isPresent()){
            Place requestedPlace = optionalPlace.get();
            requestedPlace.setUser(null);
            return requestedPlace;
        }
        else
            throw new PlaceNotFoundException("No place found with this id");
    }

    public Iterable<Place> findAllByUserId(Principal principal) {
        User retrievedUser = userRepository.findByEmail(principal.getName());
        return placeRepository.findAllByUserId(retrievedUser.getId());
    }

    public void deleteById(int placeID, Principal principal) {
        Optional<Place> placeOptional = placeRepository.findById(placeID);
        if (placeOptional.isPresent()) {
            Place place = placeOptional.get();

            if (place.getUser().getEmail().equals(principal.getName()))
                placeRepository.deleteById(placeID);
            else
                throw new AccessException("You can not access places of other users");
        }
        else
            throw new PlaceNotFoundException("No place found with this id");
    }
}
