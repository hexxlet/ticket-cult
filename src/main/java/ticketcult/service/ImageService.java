package ticketcult.service;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import ticketcult.model.Image;
import ticketcult.model.Place;
import ticketcult.repo.ImageRepository;
import ticketcult.repo.PlaceRepository;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class ImageService {

    private static final String UPLOAD_ROOT = "/home/hexxlet/Coding/Idea Projects/ticket-cult/src/main/resources/static/place-img";

    ImageRepository imageRepository;
    ResourceLoader resourceLoader;
    PlaceRepository placeRepository;

    public Resource findImage (String filename){
        return resourceLoader.getResource(UPLOAD_ROOT + "/" + filename);
    }

    public void createImage(MultipartFile file, int placeID) throws IOException {
        if (!file.isEmpty()){
            Files.copy(file.getInputStream(), Paths.get(UPLOAD_ROOT, file.getOriginalFilename()));
            Optional<Place> placeOpt = placeRepository.findById(placeID);
            placeOpt.ifPresent(place -> imageRepository.save(new Image(file.getOriginalFilename(), place)));
        }
    }

    public void deleteImage(String filename) throws IOException {
        Image toDelete = imageRepository.findByName(filename);
        imageRepository.delete(toDelete);
        Files.deleteIfExists(Paths.get(UPLOAD_ROOT, filename));
    }

    public List<String> findImagesByPlace (int placeID){
        List<String> images = new ArrayList<>();
        imageRepository.findAllByPlaceId(placeID).forEach(image -> images.add(image.getName()));
        return images;
    }
}
