package ticketcult.service;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import ticketcult.model.User;
import ticketcult.repo.UserRepository;

import java.util.ArrayList;
import java.util.List;

@Component
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class UserService implements UserDetailsService {

    UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User user = userRepository.findByEmail(username);
        if (user == null)
            throw new UsernameNotFoundException(username + " doesn't exist");

        final List<GrantedAuthority> list = new ArrayList<>();
        list.add(new SimpleGrantedAuthority(user.getRole().toString()));

        boolean isEnabled = !user.isDisabled();

        return new org.springframework.security.core.userdetails.User(
                username, user.getPasswordHash(), isEnabled,
                true, true,
                true, list);
    }

    public User loadFullUserByEmail(String email){
        return userRepository.findByEmail(email);
    }
}
