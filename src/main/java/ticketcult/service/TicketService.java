package ticketcult.service;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ticketcult.enums.Currency;
import ticketcult.enums.OrderStatus;
import ticketcult.exception.OrderException;
import ticketcult.exception.TicketTemplateException;
import ticketcult.model.Order;
import ticketcult.model.Ticket;
import ticketcult.model.TicketTemplate;
import ticketcult.repo.TicketRepository;
import ticketcult.repo.TicketTemplateRepository;

import java.time.LocalDate;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

@Component
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class TicketService {

    OrderService orderService;
    UserService userService;
    TicketTemplateRepository ticketTemplateRepository;
    TicketRepository ticketRepository;

    public void addTicketToUserOrder(int ticketTemplateID, String userEmail) {

        List<Order> allOrdersOfUser = orderService.findUserOrders(userEmail);
        boolean userHasUnpaidOrders = allOrdersOfUser.stream().anyMatch(order -> order.getStatus() == OrderStatus.UNPAID);
        if (allOrdersOfUser.isEmpty() || !userHasUnpaidOrders){
            Order order = new Order();
            order.setUser(userService.loadFullUserByEmail(userEmail));
            order.setDate(LocalDate.now());
            order.setCurrency(Currency.RUB);
            order.setStatus(OrderStatus.UNPAID);
            order.setTotalPrice(0);
            orderService.createOrUpdate(order);

            createNewTicketAndUpdateInfo(ticketTemplateID, order, userEmail);
        }
        else {
            Optional<Order> unpaidOrderOpt = allOrdersOfUser.stream().filter(order -> order.getStatus() == OrderStatus.UNPAID).findFirst();
            if (!unpaidOrderOpt.isPresent())
                throw new OrderException("No such order found");
            else {
                Order order = unpaidOrderOpt.get();
                createNewTicketAndUpdateInfo(ticketTemplateID, order, userEmail);
            }
        }
    }

    private void createNewTicketAndUpdateInfo(int ticketTemplateID, Order order, String email){
        Ticket ticket = new Ticket();
        Optional<TicketTemplate> ticketTemplate = ticketTemplateRepository.findByIdAndEnabled(ticketTemplateID, true);
        if (!ticketTemplate.isPresent())
            throw new TicketTemplateException("No such ticket template found");
        else {
            ticket.setTicketTemplate(ticketTemplate.get());
            ticket.setOrder(order);
            ticketRepository.save(ticket);

            Order orderToChange = orderService.findOrdersByStatus(OrderStatus.UNPAID, email).get(0);
            int orderID = orderToChange.getId();
            int totalPrice = orderService.calculateTotalPrice(orderID);
            orderToChange.setTotalPrice(totalPrice);
            orderService.createOrUpdate(orderToChange);
        }
    }

    public void deleteFromOrder(int ticketTemplateID, String email) {
        List<Order> orders = orderService.findOrdersByStatus(OrderStatus.UNPAID, email);
        if (!orders.isEmpty()){
            int orderID = orders.get(0).getId();
            Iterable<Ticket> ticketsOfOrder = ticketRepository.findAllByOrderId(orderID);
            Iterator<Ticket> iterator = ticketsOfOrder.iterator();
            while (iterator.hasNext()){
                Ticket ticketToDelete = iterator.next();
                if (ticketToDelete.getTicketTemplate().getId() == ticketTemplateID) {
                    ticketRepository.delete(ticketToDelete);
                    int totalPrice = orderService.calculateTotalPrice(orderID);
                    orders.get(0).setTotalPrice(totalPrice);
                    orderService.createOrUpdate(orders.get(0));
                    break;
                }
            }
        }
    }
}
