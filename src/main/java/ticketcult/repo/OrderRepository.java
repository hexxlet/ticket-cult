package ticketcult.repo;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import ticketcult.enums.OrderStatus;
import ticketcult.model.Order;

import java.util.List;
import java.util.Optional;

public interface OrderRepository extends CrudRepository<Order, Integer> {

    @Override
    <S extends Order> S save(S entity);

    @Override
    Optional<Order> findById(Integer integer);

    @Override
    Iterable<Order> findAll();

    Iterable<Order> findAllByUserEmail(String email);

    Iterable<Order> findAllByStatusAndUserEmail(OrderStatus status, String email);

    @Query(value = "select p.name_rus, p.name_eng, p.name_chn, ticket_template.description_rus, ticket_template.description_eng, ticket_template.description_chn, count(ticket_template_id) as Quantity, sum(ticket_template.price) as Price, ticket_template_id from Ticket " +
            "inner join Ticket_template on ticket.ticket_template_id = ticket_template.id " +
            "inner join Place p on ticket_template.place_id = p.id " +
            "where order_id = :orderId " +
            "group by ticket_template.description_rus, ticket_template.description_eng, ticket_template.description_chn, ticket_template_id, p.name_rus, p.name_eng, p.name_chn", nativeQuery = true)
    List<Object[]> findDeatailedOrderInfo(@Param("orderId") int orderId);
}



