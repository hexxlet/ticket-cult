package ticketcult.repo;

import org.springframework.data.repository.CrudRepository;
import ticketcult.model.Ticket;

public interface TicketRepository extends CrudRepository<Ticket, Integer> {

    Iterable<Ticket> findAllByOrderId(int orderID);

    @Override
    <S extends Ticket> S save(S entity);

    @Override
    void delete(Ticket entity);
}
