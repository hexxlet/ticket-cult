package ticketcult.repo;

import org.springframework.data.repository.CrudRepository;
import ticketcult.model.Image;

public interface ImageRepository extends CrudRepository<Image, Integer> {

    Image findByName(String name);

    Iterable<Image> findAllByPlaceId(int placeId);
}
