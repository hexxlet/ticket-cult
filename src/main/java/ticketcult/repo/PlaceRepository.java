package ticketcult.repo;

import org.springframework.data.repository.CrudRepository;
import ticketcult.model.Place;

import java.util.Optional;

public interface PlaceRepository extends CrudRepository<Place, Integer> {

    @Override
    Iterable<Place> findAll();

    @Override
    Optional<Place> findById(Integer integer);

    Iterable<Place> findAllByUserId(int id);

    @Override
    <S extends Place> S save(S entity);

    @Override
    void deleteById(Integer integer);
}
