package ticketcult.repo;

import org.springframework.data.repository.CrudRepository;
import ticketcult.model.User;

import java.util.Optional;

public interface UserRepository extends CrudRepository<User, Integer> {

    @Override
    Iterable<User> findAll();

    @Override
    Optional<User> findById(Integer integer);

    @Override
    <S extends User> S save(S entity);

    User findByEmail(String email);
}
