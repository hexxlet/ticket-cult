package ticketcult.repo;

import org.springframework.data.repository.CrudRepository;
import ticketcult.model.TicketTemplate;

import java.util.Optional;

public interface TicketTemplateRepository extends CrudRepository<TicketTemplate, Integer> {

    @Override
    <S extends TicketTemplate> S save(S entity);

    Optional<TicketTemplate> findByIdAndEnabled(Integer integer, boolean enabled);

    Iterable<TicketTemplate> findAllByPlaceIdAndEnabled(int placeID, boolean enabled);

    @Override
    void deleteById(Integer integer);

}
