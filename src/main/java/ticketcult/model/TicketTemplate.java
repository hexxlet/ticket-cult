package ticketcult.model;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import ticketcult.enums.Currency;

import javax.persistence.*;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
public class TicketTemplate {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    int id;

    @ManyToOne
    @JoinColumn(name = "place_id")
    Place place;

    @Column
    int price;

    @Enumerated(EnumType.STRING)
    @Column
    Currency currency;

    @Column(name = "description_rus")
    String descriptionRUS;

    @Column(name = "description_eng")
    String descriptionENG;

    @Column(name = "description_chn")
    String descriptionCHN;

    @Column(name = "is_enabled")
    boolean enabled;
}
