package ticketcult.model;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import ticketcult.enums.Role;

import javax.persistence.*;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@Table(name = "system_user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Access(AccessType.PROPERTY)
    int id;

    @Column
    String name;

    @Column
    String lastname;

    @Column
    String email;

    @Column(name = "hashed_password")
    String passwordHash;

    @Enumerated(EnumType.STRING)
    @Column
    Role role;

    @Column(name = "is_disabled")
    boolean isDisabled;
}
