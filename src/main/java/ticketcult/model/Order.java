package ticketcult.model;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import ticketcult.enums.Currency;
import ticketcult.enums.OrderStatus;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@Table(name = "system_order")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    int id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    User user;

    @Column(name = "date")
    LocalDate date;

    @Column(name = "total_price")
    int totalPrice;

    @Enumerated(EnumType.STRING)
    @Column
    Currency currency;

    @Enumerated(EnumType.STRING)
    @Column
    OrderStatus status;
}
