package ticketcult.model;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
public class Place {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    int id;

    @Column(name = "name_rus")
    String nameRUS;

    @Column(name = "name_eng")
    String nameENG;

    @Column(name = "name_chn")
    String nameCHN;

    @ManyToOne
    @JoinColumn(name = "owner_id")
    User user;

    @Column(name = "opening_hours_rus")
    String openingHoursRUS;

    @Column(name = "opening_hours_eng")
    String openingHoursENG;

    @Column(name = "opening_hours_chn")
    String openingHoursCHN;

    @Column(name = "description_rus")
    String descriptionRUS;

    @Column(name = "description_eng")
    String descriptionENG;

    @Column(name = "description_chn")
    String descriptionCHN;

    double latitude;

    double longitude;
}
