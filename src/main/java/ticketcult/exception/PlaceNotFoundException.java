package ticketcult.exception;

public class PlaceNotFoundException extends RuntimeException {
    public PlaceNotFoundException(String s) {
        super(s);
    }
}
