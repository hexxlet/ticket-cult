package ticketcult.exception;

public class TicketTemplateException extends RuntimeException {
    public TicketTemplateException(String message) {
        super(message);
    }
}
